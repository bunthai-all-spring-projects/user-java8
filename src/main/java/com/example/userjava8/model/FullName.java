package com.example.userjava8.model;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Embeddable;

@Embeddable
@Data
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class FullName {
    private String firstName;
    private String lastName;

    public static FullName of(String _firstName, String _lastName) {
        return new FullName(_firstName, _lastName);
    }

}
