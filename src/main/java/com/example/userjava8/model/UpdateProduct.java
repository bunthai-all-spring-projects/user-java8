package com.example.userjava8.model;

import lombok.Builder;
import lombok.Value;

import java.util.Optional;

@Value
@Builder(builderClassName = "Builder")
public class UpdateProduct {

    private FullName fullName;
    private Price price;
    private Quantity quantity;
    private Description description;

    public Optional<FullName> getFullName() { return Optional.ofNullable(fullName); }
    public Optional<Price> getPrice() { return Optional.ofNullable(price); }
    public Optional<Quantity> getQuantity() { return Optional.ofNullable(quantity); }
    public Optional<Description> getDescription() { return Optional.ofNullable(description); }

}
