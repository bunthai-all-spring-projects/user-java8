package com.example.userjava8.model;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Embeddable;

@Embeddable
@Data
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class Description {
    private String description;

    public static Description of(String _description) {
        return new Description(_description);
    }

    @Override
    public String toString() {
        return description;
    }
}
