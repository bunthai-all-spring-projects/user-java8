package com.example.userjava8.model;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Embeddable;

@Embeddable
@Data
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class Price {

    private double price;

    public static Price of(double _price) {
        return new Price(_price);
    }

    @Override
    public String toString() {
        return String.valueOf(price);
    }
}
