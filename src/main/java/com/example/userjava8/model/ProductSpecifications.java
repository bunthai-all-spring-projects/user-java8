//package com.example.userjava8.model;
//
//import org.springframework.data.jpa.domain.Specification;
//
//import javax.persistence.criteria.CriteriaBuilder;
//import javax.persistence.criteria.CriteriaQuery;
//import javax.persistence.criteria.Root;
//import java.util.Collection;
//import java.util.List;
//import java.util.stream.Collectors;
//
//public class ProductSpecifications {
//    public static Specification<Product> idIn(Collection<ProductId> _ids)
//    {
//        return (Root<Product> _root, CriteriaQuery<?> _query, CriteriaBuilder _cb) ->
//        {
//            if (_ids.isEmpty()) {
//                return _cb.or();  // orは常にfalseとして評価する
//            }
//
//            List<Long> ids = _ids.stream().map(tableId -> tableId.getTableId()).collect(Collectors.toList());
//            return _root.get("tableId").in(ids);
//        };
//    }
//}
