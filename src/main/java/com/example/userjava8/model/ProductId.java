package com.example.userjava8.model;

import lombok.*;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import javax.persistence.Embeddable;
import java.io.Serializable;

//@Embeddable
@Data
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class ProductId implements Serializable {
    private long tableId;

    public static ProductId of(long _productId) {
        return new ProductId(_productId);
    }

    @Override
    public String toString() {
        return String.valueOf(tableId);
    }

//    @Converter(autoApply = true)
//    public static class JpaConverter implements AttributeConverter<ProductId,Long>
//    {
//        @Override
//        public Long convertToDatabaseColumn(ProductId _attribute)
//        {
//            return _attribute.tableId;
//        }
//
//        @Override
//        public ProductId convertToEntityAttribute(Long _dbData)
//        {
//            return ProductId.of(_dbData);
//        }
//    }

}
