package com.example.userjava8.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@IdClass(ProductId.class)
public class Product {


    @Id
    @SequenceGenerator(name = "operator_seq", sequenceName = "operator_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "operator_seq")
//    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "operator_seq")
    @Column(name = "id")
    private long tableId;

    @Transient
    private ProductId id;
    private FullName fullName;
    private Price price;
    private Quantity quantity;
    private Description description;

    public static Product of(FullName fullName, Price price, Quantity quantity, Description description) {
        return new Product(fullName, price, quantity, description);
    }

    private Product(FullName fullName, Price price, Quantity quantity, Description description) {
        this.fullName = fullName;
        this.price = price;
        this.quantity = quantity;
        this.description = description;
    }

    private Product(ProductId _id, FullName fullName, Price price, Quantity quantity, Description description) {
        if (_id != null) {
            this.tableId = Long.parseLong(_id.toString());
            this.id = _id;
        }
        this.fullName = fullName;
        this.price = price;
        this.quantity = quantity;
        this.description = description;
    }

    public Product withUpdate(UpdateProduct _update) {
        System.out.println(_update.getPrice().isPresent());
        System.out.println(price);
        return new Product(
                id,
                _update.getFullName().orElse(fullName),
                _update.getPrice().orElse(price),
                _update.getQuantity().orElse(quantity),
                _update.getDescription().orElse(description)

        );
    }

    @PostLoad
    private void setupId() {
        this.id = ProductId.of(this.tableId);
    }

}
