package com.example.userjava8.model;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Embeddable;

@Embeddable
@Data
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class Quantity {

    private int quatity;

    public static Quantity of(int _quantity){
        return new Quantity(_quantity);
    }

    @Override
    public String toString() {
        return String.valueOf(quatity);
    }
}
