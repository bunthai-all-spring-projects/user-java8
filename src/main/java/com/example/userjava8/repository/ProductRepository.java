package com.example.userjava8.repository;

import com.example.userjava8.model.Product;
import com.example.userjava8.model.ProductId;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ProductRepository extends JpaRepository<Product, ProductId> {
    Optional<Product> findById(ProductId id);
}
