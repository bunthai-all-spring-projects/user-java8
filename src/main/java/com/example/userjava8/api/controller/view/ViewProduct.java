package com.example.userjava8.api.controller.view;

import com.example.userjava8.model.FullName;
import com.example.userjava8.model.Product;
import com.example.userjava8.model.ProductId;
import lombok.Value;

@Value
public class ViewProduct {

    private String id;
    private String firstName;
    private String lastName;
    private String price;
    private String quantity;
    private String description;

    public ViewProduct(Product product) {
        this.id = String.valueOf(product.getId());
        this.firstName = String.valueOf(product.getFullName().getFirstName());
        this.lastName = String.valueOf(product.getFullName().getLastName());
        this.price = String.valueOf(product.getPrice());
        this.quantity = String.valueOf(product.getQuantity());
        this.description = String.valueOf(product.getDescription());
    }
}
