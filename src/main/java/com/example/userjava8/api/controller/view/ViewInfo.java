package com.example.userjava8.api.controller.view;

import lombok.Data;

import java.util.List;

@Data
public class ViewInfo<T> {
    private List<T> items;
    private int offset;
    private int limit;
    private int total;

    public ViewInfo(List<T> items, int offset, int limit, int total) {
        this.items = items;
        this.offset = offset;
        this.limit = limit;
        this.total = total;
    }

}
