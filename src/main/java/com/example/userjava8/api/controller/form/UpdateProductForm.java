package com.example.userjava8.api.controller.form;

import com.example.userjava8.model.Description;
import com.example.userjava8.model.FullName;
import com.example.userjava8.model.Price;
import com.example.userjava8.model.Quantity;
import lombok.Value;

import java.util.Optional;

@Value
public class UpdateProductForm {
    private String firstName;
    private String lastName;
    private String price;
    private String quantity;
    private String description;

    public Optional<String> getFirstName() { return Optional.ofNullable(firstName); }
    public Optional<String> getLastName() { return Optional.ofNullable(lastName); }
    public Optional<FullName> getFullName(FullName fullName) {

        return Optional.ofNullable(FullName.of(getFirstName().orElse(fullName.getFirstName()),
                getLastName().orElse(fullName.getLastName())));
    }
    public Optional<String> getPrice() { return Optional.ofNullable(price); }
    public Optional<String> getQuantity() { return Optional.ofNullable(quantity); }
    public Optional<String> getDescription() { return Optional.ofNullable(description); }


}
