package com.example.userjava8.api.controller.form;

import lombok.Data;

@Data
public class CreateProductForm {

    private String firstName;
    private String lastName;
    private double price;
    private int qty;
    private String description;

}
