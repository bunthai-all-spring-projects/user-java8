package com.example.userjava8.api.controller;

import com.example.userjava8.api.controller.form.CreateProductForm;
import com.example.userjava8.api.controller.form.UpdateProductForm;
import com.example.userjava8.api.controller.view.ViewInfo;
import com.example.userjava8.api.controller.view.ViewProduct;
import com.example.userjava8.model.*;
import com.example.userjava8.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityNotFoundException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collector;
import java.util.stream.Collectors;

@RestController
@RequestMapping("v1/products")
public class ProductRestController {

    @Autowired
    ProductRepository productRepository;

    @PostMapping
    public ResponseEntity<ViewProduct> createProduct(
        @RequestBody CreateProductForm _product
    ) {
        FullName fullName = FullName.of(_product.getFirstName(), _product.getLastName());
        Price price = Price.of(_product.getPrice());
        Quantity quantity = Quantity.of(_product.getQty());
        Description description = Description.of(_product.getDescription());

        Product product = Product.of(fullName, price, quantity, description);
        Product persistProduct = productRepository.save(product);
        ViewProduct viewProduct = new ViewProduct(persistProduct);
        return new ResponseEntity<>(viewProduct, HttpStatus.OK);

    }

    @GetMapping
    public ViewInfo<ViewProduct> getProducts(
        @RequestParam(value = "offset", required = false) String _offset,
        @RequestParam(value = "limit", required = false) String _limit
    ) {
        if (_offset == null) _offset = "0";
        if (_limit == null) _limit = "20";
        Pageable pageable = PageRequest.of(Integer.parseInt(_offset), Integer.parseInt(_limit));
        Page<Product> productPage = productRepository.findAll(pageable);
        List<ViewProduct> products = new ArrayList<>();
        productPage.get().forEach(product -> products.add(new ViewProduct(product)));

        ViewInfo<ViewProduct> viewInfo = new ViewInfo<>(products, Integer.parseInt(_offset), Integer.parseInt(_limit), productPage.getTotalPages());

        return viewInfo;
    }

    @GetMapping("{id}")
    public ResponseEntity<ViewProduct> getProductById(
        @PathVariable("id") long _id
    ) throws Exception {
        Optional<Product> product = productRepository.findById(ProductId.of(_id));

        if (product.isPresent()) {
            return new ResponseEntity<>(new ViewProduct(product.get()), HttpStatus.OK);
        } else {
            throw new Exception("Product Not found");
        }

    }

    @PatchMapping("{id}")
    public ResponseEntity<ViewProduct> updateProduct(
        @PathVariable("id") long _id,
        @RequestBody UpdateProductForm _form
        ) {

        UpdateProduct.Builder update = UpdateProduct.builder();

        FullName currentFullName = productRepository.findById(ProductId.of(_id)).get().getFullName();

        _form.getFullName(currentFullName).ifPresent(fullName -> update.fullName(fullName));
        _form.getPrice().ifPresent(price -> update.price(Price.of(Double.parseDouble(price))));
        _form.getQuantity().ifPresent(qty -> update.quantity(Quantity.of(Integer.parseInt(qty))));
        _form.getDescription().ifPresent(des -> update.description(Description.of(des)));

        Product productCheck = productRepository.findById(ProductId.of(_id)).orElseThrow(() -> new EntityNotFoundException());
        Product productUpdate = productCheck.withUpdate(update.build());

        System.out.println(productUpdate);
        Product product = productRepository.save(productUpdate);
        ViewProduct viewProduct = new ViewProduct(product);
        return new ResponseEntity<>(viewProduct, HttpStatus.OK);

    }

}
