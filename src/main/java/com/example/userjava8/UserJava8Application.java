package com.example.userjava8;

import com.example.userjava8.model.FullName;
import com.example.userjava8.model.Price;
import com.example.userjava8.model.Product;
import com.example.userjava8.model.Quantity;
import com.example.userjava8.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.ArrayList;
import java.util.List;

@SpringBootApplication
public class UserJava8Application implements CommandLineRunner {

	public static void main(String[] args) {
		SpringApplication.run(UserJava8Application.class, args);
	}

	@Autowired
	ProductRepository repository;

	@Override
	public void run(String... args) throws Exception {

		List<Product> products = new ArrayList<>();
		for (int i = 1; i <= 1000; i++) {
			Product p = new Product();

			int nameLength = (int )(Math.random() * (10-3))+3;
			String firstName = "";
			String lastName = "";
			for (int j = 1; j <= nameLength; j++) {
				if (j == 1) {
					firstName += (char) ((int) (Math.random() * (90 - 65)) + 65);
					lastName += (char) ((int) (Math.random() * (90 - 65)) + 65);
				}
				else {
					firstName += (char) ((int) (Math.random() * (97 - 122)) + 122);
					lastName += (char) ((int) (Math.random() * (97 - 122)) + 122);
				}

			}

			double price = (int )(Math.random() * (150-10))+10;
			int qty = (int )(Math.random() * (100-1))+1;
			p.setFullName(FullName.of(firstName, lastName));
			p.setPrice(Price.of(price));
			p.setQuantity(Quantity.of(qty));
			products.add(p);
		}

		repository.saveAll(products);

	}
}
